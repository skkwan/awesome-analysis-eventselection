See [here](https://awesome-workshop.github.io/awesome-htautau-analysis/) for the documentation.

At the start of each session:
```
source /cvmfs/sft.cern.ch/lcg/views/LCG_95/x86_64-centos7-gcc8-opt/setup.sh
```
